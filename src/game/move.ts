import {Position} from "../utils/position";
import {MaybePiece} from "./pieces/piece/piece";

/**
 * Class containing information about a move
 */
export class Move {
    /**
     * The position the piece departs from in the move
     */
    public departurePosition: Position;

    /**
     * The position that is the destination of the move
     */
    public destinationPosition: Position;

    /**
     * The piece that was captured during the move, or null if one was taken
     */
    public pieceCaptured: MaybePiece = null;

    /**
     * If the piece that was moved was the first move by the piece
     */
    public wasFirstMove: boolean;

    /**
     * @param departurePosition - The position the piece departs from in the move
     * @param destinationPosition - The position that is the destination of the move
     * @param pieceCaptured - The piece that was captured during the move, or null if one was taken
     * @param wasFirstMove - If the piece that was moved was the first move by the piece
     */
    constructor(departurePosition: Position, destinationPosition: Position,
                pieceCaptured: MaybePiece = null, wasFirstMove: boolean = false) {
        this.departurePosition = departurePosition;
        this.destinationPosition = destinationPosition;
        this.wasFirstMove = wasFirstMove;
        this.pieceCaptured = pieceCaptured;
    }
}
