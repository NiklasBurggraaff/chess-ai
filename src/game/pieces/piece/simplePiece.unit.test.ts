import {King} from "./implementations/king";
import {Color} from "./piece";

describe('Line Move Piece class', function () {
    test('initialiseSimplePiece', function () {
        let king = new King("A1", Color.white);
        expect(typeof king).toBe("object");
    });
});
