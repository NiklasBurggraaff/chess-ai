import {Position, Direction} from "../../../utils/position";
import {MaybePiece, Piece} from "./piece";
import {Move} from "../../move";
import {Pieces} from "../pieces";

/**
 * Class containing the information about a piece
 */
export abstract class SimplePiece extends Piece {
    /**
     * The directions of the lines that the piece can move
     */
    abstract stepDirections: Direction[];

    /**
     * The maximum number of steps that the piece can moves
     */
    protected maxSteps: number = Infinity;

    /**
     * Directions for straight steps
     */
    protected straightStepDirections: Direction[] = [
        new Direction(1, 0),
        new Direction(-1, 0),
        new Direction(0, 1),
        new Direction(0, -1),
    ];

    /**
     * Directions for diagonal steps
     */
    protected diagonalStepDirections: Direction[] = [
        new Direction(1, 1),
        new Direction(1, -1),
        new Direction(-1, 1),
        new Direction(-1, -1),
    ];

    /**
     * Direction for knight steps
     */
    protected knightStepDirections: Direction[] = [
        new Direction(1, 2),
        new Direction(2, 1),
        new Direction(1, -2),
        new Direction(2, -1),
        new Direction(-1, 2),
        new Direction(-2, 1),
        new Direction(-1, -2),
        new Direction(-2, -1),
    ];

    /**
     * @param pieces - The pieces
     * @returns The possible moves the piece can make
     */
    possibleMoves(pieces: Pieces): Move[] {
        let moves: Move[] = [];

        for (let i = 0; i < this.stepDirections.length; i++) {
            this.addPositionsInStepDirection(pieces, moves, this.stepDirections[i]);
        }

        return moves;
    }

    /**
     * Add the move to the given moves array if the move is valid
     * @param pieces - The pieces
     * @param moves - The array where the move should be added to
     * @param stepDirection - The direction vector of the step to add moves in
     */
    protected addPositionsInStepDirection(pieces: Pieces, moves: Move[], stepDirection: Direction) {
        let destinationPosition: Position = this.position.getMovedPosition(stepDirection);

        let steps: number = 0;

        while (steps < this.maxSteps && this.isValidMoveDestination(pieces, destinationPosition)) {
            let capturedPiece: MaybePiece = pieces.getPiece(destinationPosition);

            let move = new Move(this.position, destinationPosition, capturedPiece, !this.hasMoved);

            // Add move to the moves array
            moves.push(move);

            // If the piece captures a piece it cannot move further in that direction
            if (capturedPiece != null) {
                break;
            }

            // Move the position
            destinationPosition = destinationPosition.getMovedPosition(stepDirection);
            steps += 1;
        }
    }
}
