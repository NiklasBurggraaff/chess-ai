import {Position} from "../../../utils/position";
import {Move} from "../../move";
import {Pieces} from "../pieces";

/**
 * Class containing the information about a piece
 */
export abstract class Piece {
    /**
     * The number of points that the piece is worth
     */
    protected abstract piecePoints: number;

    /**
     * The initials of the piece to be put on the piece
     */
    protected abstract initials: string;

    /**
     * The position of the piece
     */
    protected position: Position;

    /**
     * @returns The position of the piece
     */
    getPosition(): Position {
        return this.position;
    }

    /**
     * True if the piece is white, and false if the piece is black
     */
    public color: Color;

    /**
     * True if the piece has moved before in the game, and false if it has not
     */
    protected hasMoved: boolean;

    /**
     * @param position - The position of the piece as a position or squareId string
     * @param color - The color of the piece
     * @param hasMoved - True if the piece should behave as if it has already moved
     */
    constructor(position: Position | string, color: Color, hasMoved: boolean = false) {
        if (position instanceof Position) {
            this.position = position;
        } else {
            this.position = Position.atSquareId(position)
        }

        if (!this.position.isValid()) {
            throw new Error("The given position of the piece is not valid.");
        }

        this.color = color;
        this.hasMoved = hasMoved;
    }

    /**
     * @returns HTML of the piece
     */
    getHTML(): string {
        return `<div class="piece ${Color[this.color]}">${this.initials}</div>`;
    }

    /**
     * @returns The number of points of the piece
     */
    points(): number {
        let points: number = 0;

        switch (this.color) {
            case Color.white:
                points += this.piecePoints;
                break;
            case Color.black:
                points -= this.piecePoints;
                break;
        }

        return points;
    }

    /**
     * Method called when a piece has been moved
     *
     * @param move - The move that the piece needs to make
     */
    move(move: Move) {
        if (!this.hasMoved) {
            this.hasMoved = true;
        }

        if (!this.position.isEqualTo(move.departurePosition)) {
            throw new Error("The departure square is not the same as the position of the piece.");
        }

        this.position = move.destinationPosition;
    }

    /**
     * @param pieces - The pieces
     * @returns The possible moves the piece can make
     */
    abstract possibleMoves(pieces: Pieces): Move[];

    /**
     * Checks if the given position is a valid destination of a move or not
     * @param pieces - The pieces
     * @param destinationPosition - The position to check
     * @param canCapture - If the move can be a capture, true by default
     * @returns True if the move is valid to and false otherwise
     */
    protected isValidMoveDestination(pieces: Pieces, destinationPosition: Position, canCapture: boolean = true): boolean {
        if (!destinationPosition.isValid()) {
            // If the destination position  is not valid it cannot be a valid move
            return false;
        }

        let destinationPiece: MaybePiece = pieces.getPiece(destinationPosition);

        if (destinationPiece == null) {
            // If there is no piece, the move is valid
            return true;
        }

        if (canCapture) {
            // Can capture the piece if it not the same color
            return destinationPiece.color != this.color;
        }

        // Cannot capture piece at the destination position
        return false;
    }

    /**
     * @returns A string which represents the piece
     */
    toString(): string {
        return `${Color[this.color]} ${this.initials}`;
    }

    /**
     * Checks if the given piece is equal to this piece
     * @param maybePiece - The piece to compare this to
     * @returns True if the pieces are equal, and false otherwise
     */
    isEqualTo(maybePiece: MaybePiece): boolean {
        if (!maybePiece) {
            return false;
        }

        let piece: Piece = maybePiece as Piece;

        let colorsEqual = this.color == piece.color;
        let initialsEqual = this.initials == piece.initials;
        let positionsEqual = this.position.isEqualTo(piece.position);
        let hasMovedEqual = this.hasMoved == piece.hasMoved
        return colorsEqual && initialsEqual && positionsEqual && hasMovedEqual;
    }
}

/**
 * Enumerator for the possible colors of a piece
 */
export enum Color {
    white,
    black
}

export type MaybePiece = Piece | null;
