import {Pawn} from "./implementations/pawn";
import {Color, MaybePiece} from "./piece";
import {Rook} from "./implementations/rook";
import {Position} from "../../../utils/position";
import {Move} from "../../move";
import {PositionsComparator} from "./positionsComparator";
import {Pieces} from "../pieces";

describe('Piece class', function () {
    describe('Constructor', function () {
        test('Passing and invalid position to the Pawn constructor throws an error', function () {
            let invalidPosition = new Position(0, -1);
            expect(invalidPosition.isValid()).toBeFalsy();

            expect(function () {
                new Pawn(invalidPosition, Color.white);
            }).toThrow("The given position of the piece is not valid.");
        });
        test('Passing and invalid squareId to the Pawn constructor throws an error', function () {
            let invalidSquareId = "H9";
            expect(function () {
                Position.atSquareId(invalidSquareId).isValid();
            }).toThrow("The given squareId string \"" + invalidSquareId + "\" is not valid.");

            expect(function () {
                new Pawn(invalidSquareId, Color.white);
            }).toThrow("The given squareId string \"" + invalidSquareId + "\" is not valid.");
        });
    });

    describe('Move', function () {
        test('The position of a piece after a move is the destination position', function () {
            let departurePosition = Position.atSquareId("A2");
            let destinationPosition = Position.atSquareId("A3");

            let pawn = new Pawn(departurePosition, Color.white);
            let move = new Move(departurePosition, destinationPosition);

            expect(pawn.getPosition().isEqualTo(departurePosition)).toBeTruthy();

            pawn.move(move);

            expect(pawn.getPosition().isEqualTo(destinationPosition)).toBeTruthy();
        });
        test('After a piece is moved the hasMoved property is true', function () {
            let pieces = new Pieces();

            let departurePosition = Position.atSquareId("A2");
            let destinationPosition = Position.atSquareId("A3");

            let pawn = new Pawn(departurePosition, Color.white);
            pieces.addPiece(pawn);
            let move = new Move(departurePosition, destinationPosition);

            pawn.move(move);

            let possibleMoves: Move[] = pawn.possibleMoves(pieces);
            // The pawn can only move one step forwards because it has already moved
            let expectedMoveDestinations: string[] = ["A4"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('After a piece that has already moved is moved the hasMoved property is still true', function () {
            let pieces = new Pieces();

            let departurePosition = Position.atSquareId("A3");
            let destinationPosition = Position.atSquareId("A4");

            let pawn = new Pawn(departurePosition, Color.white, true);
            pieces.addPiece(pawn);
            let move = new Move(departurePosition, destinationPosition, null, true);

            let possibleMoves: Move[] = pawn.possibleMoves(pieces);
            // The pawn can only move one step forwards because it has already moved
            let expectedMoveDestinations: string[] = ["A4"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);

            pawn.move(move);

            possibleMoves = pawn.possibleMoves(pieces);
            // The pawn can only move one step forwards because it has already moved
            expectedMoveDestinations = ["A5"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('If the departure position is not the same as the position of the square an error is thrown', function () {
            let pawn = new Pawn("A2", Color.white);
            let move = new Move(Position.atSquareId("A3"), Position.atSquareId("A4"));

            expect(function () {
                pawn.move(move);
            }).toThrow("The departure square is not the same as the position of the piece.");
        });
    });

    describe('Get HTML', function () {
        test('HTML object of a white pawn has the correct white class and the correct Pa initials', function () {
            let piece = new Pawn("A2", Color.white);
            let color = "white";
            let initials = "Pa";
            expect(piece.getHTML()).toBe(`<div class="piece ${color}">${initials}</div>`)
        });
        test('HTML object of a black pawn has the correct black class and the correct Pa initials', function () {
            let piece = new Pawn("A2", Color.black);
            let color = "black";
            let initials = "Pa";
            expect(piece.getHTML()).toBe(`<div class="piece ${color}">${initials}</div>`)
        });
        test('HTML object of a white rook has the correct white class and the correct Ro initials', function () {
            let piece = new Rook("A1", Color.white);
            let color = "white";
            let initials = "Ro";
            expect(piece.getHTML()).toBe(`<div class="piece ${color}">${initials}</div>`)
        });
        test('HTML object of a black rook has the correct black class and the correct Ro initials', function () {
            let piece = new Rook("A1", Color.black);
            let color = "black";
            let initials = "Ro";
            expect(piece.getHTML()).toBe(`<div class="piece ${color}">${initials}</div>`)
        });
    });

    describe('Is equal to', function () {
        test('A piece is equal to itself', function () {
            let piece = new Rook("A1", Color.white);

            expect(piece.isEqualTo(piece)).toBeTruthy();
        });
        test('Pieces at different positions are NOT equal', function () {
            let piece1 = new Rook("A1", Color.white);
            let piece2 = new Rook("A2", Color.white);

            expect(piece1.isEqualTo(piece2)).toBeFalsy();
        });
        test('Pieces of different colors are NOT equal', function () {
            let piece1 = new Rook("A1", Color.white);
            let piece2 = new Rook("A1", Color.black);

            expect(piece1.isEqualTo(piece2)).toBeFalsy();
        });
        test('The same piece, where on has moved are NOT equal', function () {
            let piece1 = new Rook("A1", Color.white);
            let piece2 = new Rook("A1", Color.black, true);

            expect(piece1.isEqualTo(piece2)).toBeFalsy();
        });
        test('A piece is NOT equal to null', function () {
            let piece1 = new Rook("A1", Color.white);
            let piece2: MaybePiece = null;

            expect(piece1.isEqualTo(piece2)).toBeFalsy();
        });
    });
});
