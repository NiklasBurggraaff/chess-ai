import {Color} from "../piece";
import {Rook} from "./rook";
import {Game} from "../../../game";
import {Pawn} from "./pawn";
import {PositionsComparator} from "../positionsComparator"

describe("Rook class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White rook is worth 5 points', function () {
            let rook = new Rook("A1", Color.white);
            game.pieces.addPiece(rook);
            expect(rook.points()).toBe(5);
        });
        test('Black rook is worth -5 points', function () {
            let rook = new Rook("A1", Color.black);
            game.pieces.addPiece(rook);
            expect(rook.points()).toBe(-5);
        });
    });

    describe('Possible move positions', function () {
        test('White rook on A1 can move along D file and 1st rank', function () {
            let rook = new Rook("A1", Color.white);
            game.pieces.addPiece(rook);
            let possibleMoves = rook.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["A2", "A3", "A4", "A5", "A6", "A7", "A8",
                    "B1", "C1", "D1", "E1", "F1", "G1", "H1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black rook on A8 can move along D file and 8th rank', function () {
            let rook = new Rook("A8", Color.black);
            game.pieces.addPiece(rook);
            let possibleMoves = rook.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["A1", "A2", "A3", "A4", "A5", "A6", "A7",
                    "B8", "C8", "D8", "E8", "F8", "G8", "H8"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White rook on A1 with white pawn on A2 can NOT move along A file', function () {
            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);

            let rook = new Rook("A1", Color.white);
            game.pieces.addPiece(rook);
            let possibleMoves = rook.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["B1", "C1", "D1", "E1", "F1", "G1", "H1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White rook on A1 with black pawn on A2 can capture on A2 and move along 1st rank', function () {
            let pawn = new Pawn("A2", Color.black);
            game.pieces.addPiece(pawn);

            let rook = new Rook("A1", Color.white);
            game.pieces.addPiece(rook);
            let possibleMoves = rook.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["A2", "B1", "C1", "D1", "E1", "F1", "G1", "H1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
    });

    test('toString_white_white Ro', function () {
        let rook = new Rook("A1", Color.white);
        game.pieces.addPiece(rook);
        expect(rook.toString()).toBe("white Ro")
    });
    test('toString_black_black Ro', function () {
        let rook= new Rook("A1", Color.black);
        game.pieces.addPiece(rook);
        expect(rook.toString()).toBe("black Ro")
    });
});
