import {Direction} from "../../../../utils/position";
import {SimplePiece} from "../simplePiece";

/**
 * Class containing information about the rook piece
 */
// TODO: Try to fix this branch not covered rather than ignore it
/* istanbul ignore next */
export class Rook extends SimplePiece {
    /**
     * The number of points that the piece is worth
     */
    piecePoints: number = 5;

    /**
     * The initials of the piece to be put on the piece
     */
    initials: string = "Ro";

    /**
     * The directions of the lines that the piece can move
     */
    stepDirections: Direction[] = this.straightStepDirections;
}
