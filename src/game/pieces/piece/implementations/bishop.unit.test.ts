import {Color} from "../piece";
import {Bishop} from "./bishop";
import {Game} from "../../../game";
import {Pawn} from "./pawn";
import {PositionsComparator} from "../positionsComparator"

describe("Bishop class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White bishop is worth 3 points', function () {
            let bishop = new Bishop("A1", Color.white);
            game.pieces.addPiece(bishop);
            expect(bishop.points()).toBe(3);
        });
        test('Black bishop is worth -3 points', function () {
            let bishop = new Bishop("A1", Color.black);
            game.pieces.addPiece(bishop);
            expect(bishop.points()).toBe(-3);
        });
    });

    describe('Possible move positions', function () {
        test('White bishop on C1 can move diagonally left and right', function () {
            let bishop = new Bishop("C1", Color.white);
            game.pieces.addPiece(bishop);
            let possibleMoves = bishop.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["A3", "B2", "D2", "E3", "F4", "G5", "H6"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black bishop on C8 can move diagonally left and right', function () {
            let bishop = new Bishop("C8", Color.black, true);
            game.pieces.addPiece(bishop);
            let possibleMoves = bishop.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] =
                ["A6", "B7", "D7", "E6", "F5", "G4", "H3"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White bishop on C1 with a white pawn on D2 can NOT move diagonally right', function () {
            let pawn = new Pawn("D2", Color.white);
            game.pieces.addPiece(pawn);

            let bishop = new Bishop("C1", Color.white);
            game.pieces.addPiece(bishop);
            let possibleMoves = bishop.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "B2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White bishop on C1 with a black pawn on D2 can capture diagonally right', function () {
            let pawn = new Pawn("D2", Color.black);
            game.pieces.addPiece(pawn);

            let bishop = new Bishop("C1", Color.white);
            game.pieces.addPiece(bishop);
            let possibleMoves = bishop.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "B2", "D2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
    });

    describe('To string', function () {
        test('White bishop to string is white Bi', function () {
            let bishop = new Bishop("A1", Color.white);
            game.pieces.addPiece(bishop);
            expect(bishop.toString()).toBe("white Bi")
        });
        test('Black bishop to string is black Bi', function () {
            let bishop= new Bishop("A1", Color.black);
            game.pieces.addPiece(bishop);
            expect(bishop.toString()).toBe("black Bi")
        });
    });
});
