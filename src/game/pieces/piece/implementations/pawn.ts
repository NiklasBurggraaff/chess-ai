import {Color, Piece} from "../piece";
import {Position, Direction} from "../../../../utils/position";
import {Move} from "../../../move";
import {Pieces} from "../../pieces";

/**
 * Class containing information about the pawn piece
 */
export class Pawn extends Piece {
    /**
     * The number of points that the piece is worth
     */
    piecePoints: number = 1;

    /**
     * The initials of the piece to be put on the piece
     */
    initials: string = "Pa";

    /**
     * @param pieces - The pieces
     * @returns The possible positions that the piece can move to
     */
    possibleMoves(pieces: Pieces): Move[] {
        let moves: Move[] = [];

        let deltaRank = this.getDeltaRank();

        // Simple single step
        let singleStep: Position = this.position.getMovedPosition(new Direction(0, deltaRank));

        let canDoSingleStepMove = this.isValidMoveDestination(pieces, singleStep, false);
        if (canDoSingleStepMove) {
            let singleStepMove = new Move(this.position, singleStep, null, !this.hasMoved);
            moves.push(singleStepMove);
        }

        // The pawn can only make a double step if it has not hasMoved yet
        if (canDoSingleStepMove && !this.hasMoved) {
            let doubleStep: Position = this.position.getMovedPosition(new Direction(0, deltaRank * 2));

            if (this.isValidMoveDestination(pieces, doubleStep)) {
                let doubleStepMove = new Move(this.position, doubleStep, null, !this.hasMoved);
                moves.push(doubleStepMove);
            }
        }

        // Capturing
        this.addCaptureMovePositionIfValid(pieces, moves, new Direction(-1, deltaRank));
        this.addCaptureMovePositionIfValid(pieces, moves, new Direction(1, deltaRank));

        return moves;
    }

    /**
     * Add the move to the movePosition if the given direction is a valid capture move for the pawn.
     * This means that the direction is a diagonal step and
     * there is a piece of the opposite color in the position in that direction.
     *
     * @param pieces - The pieces
     * @param moves - The moves that the move should be added to
     * @param direction - The direction to check if there is a valid capture move
     */
    private addCaptureMovePositionIfValid(pieces: Pieces, moves: Move[], direction: Direction) {
        let diagonalPosition: Position = this.position.getMovedPosition(direction);

        if (!diagonalPosition.isValid()) {
            return;
        }

        let diagonalPiece = pieces.getPiece((diagonalPosition as Position));

        if (diagonalPiece != null && diagonalPiece.color != this.color) {
            let captureMove = new Move(this.position, diagonalPosition, diagonalPiece, !this.hasMoved);
            moves.push(captureMove);
        }
    }

    /**
     * @returns The delta rank that the pawn can move in
     */
    private getDeltaRank(): number {
        switch (this.color) {
            case Color.white:
                return 1;
            case Color.black:
                return -1;
        }
    }
}
