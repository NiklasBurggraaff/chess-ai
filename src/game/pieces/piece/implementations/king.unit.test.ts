import {Color} from "../piece";
import {King} from "./king";
import {Game} from "../../../game";
import {Pawn} from "./pawn";
import {PositionsComparator} from "../positionsComparator"

describe("King class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White king is worth 1000 points', function () {
            let king = new King("A1", Color.white);
            game.pieces.addPiece(king);
            expect(king.points()).toBe(1000);
        });
        test('Black king is worth -1000 points', function () {
            let king = new King("A1", Color.black);
            expect(king.points()).toBe(-1000);
        });
    });

    describe('Possible move positions', function () {
        test('White king on E1 can move to the 5 positions around it', function () {
            let king = new King("E1", Color.white);
            game.pieces.addPiece(king);
            let possibleMoves = king.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["D1", "D2", "E2", "F2", "F1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black king on E8 can move to the 5 positions around it', function () {
            let king = new King("E8", Color.black);
            game.pieces.addPiece(king);
            let possibleMoves = king.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["D8", "D7", "E7", "F7", "F8"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White king on E1 with a white pawn on E2 can NOT move to that position', function () {
            let pawn = new Pawn("E2", Color.white);
            game.pieces.addPiece(pawn);

            let king = new King("E1", Color.white);
            game.pieces.addPiece(king);
            let possibleMoves = king.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["D1", "D2", "F2", "F1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White king on E1 with a black pawn on E1 can move to that position', function () {
            let pawn = new Pawn("E2", Color.black);
            game.pieces.addPiece(pawn);

            let king = new King("E1", Color.white);
            game.pieces.addPiece(king);
            let possibleMoves = king.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["D1", "D2", "E2", "F2", "F1"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
    });

    describe('To string', function () {
        test('White king to string is white Ki', function () {
            let king = new King("A1", Color.white);
            game.pieces.addPiece(king);
            expect(king.toString()).toBe("white Ki")
        });
        test('Black king to string is black Ki', function () {
            let king= new King("A1", Color.black);
            game.pieces.addPiece(king);
            expect(king.toString()).toBe("black Ki")
        });
    });
});
