import {Color} from "../piece";
import {Queen} from "./queen";
import {Game} from "../../../game";
import {Pawn} from "./pawn";
import {PositionsComparator} from "../positionsComparator"

describe("Queen class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White queen is worth 10 points', function () {
            let queen = new Queen("A1", Color.white);
            game.pieces.addPiece(queen);
            expect(queen.points()).toBe(10);
        });
        test('Black queen is worth -10 points', function () {
            let queen = new Queen("A1", Color.black);
            expect(queen.points()).toBe(-10);
        });
    });

    describe('Possible move positions', function () {
        test('White queen on D1 can move along D file, 1st rank and diagonally', function () {
            let queen = new Queen("D1", Color.white, true);
            game.pieces.addPiece(queen);
            let possibleMoves = queen.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [
                "A1", "B1", "C1", "E1", "F1", "G1", "H1",
                "D2", "D3", "D4", "D5", "D6", "D7", "D8",
                "A4", "B3", "C2", "E2", "F3", "G4", "H5"
            ];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black queen on D8 can move along D file, 8th rank and diagonally', function () {
            let queen = new Queen("D8", Color.white);
            game.pieces.addPiece(queen);
            let possibleMoves = queen.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [
                "A8", "B8", "C8", "E8", "F8", "G8", "H8",
                "D7", "D6", "D5", "D4", "D3", "D2", "D1",
                "A5", "B6", "C7", "E7", "F6", "G5", "H4"
            ];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White queen on D1 with a white pawn in front can NOT move along D file', function () {
            let pawn = new Pawn("D2", Color.white);
            game.pieces.addPiece(pawn);

            let queen = new Queen("D1", Color.white);
            game.pieces.addPiece(queen);
            let possibleMoves = queen.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [
                "A1", "B1", "C1", "E1", "F1", "G1", "H1",
                "A4", "B3", "C2", "E2", "F3", "G4", "H5"
            ];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White queen on D1 with a white pawn on C2 can NOT move diagonally left', function () {
            let pawn = new Pawn("C2", Color.white);
            game.pieces.addPiece(pawn);

            let queen = new Queen("D1", Color.white);
            game.pieces.addPiece(queen);
            let possibleMoves = queen.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [
                "A1", "B1", "C1", "E1", "F1", "G1", "H1",
                "D2", "D3", "D4", "D5", "D6", "D7", "D8",
                "E2", "F3", "G4", "H5"
            ];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
    });

    describe('To string', function () {
        test('White queen to string is white Qu', function () {
            let queen = new Queen("A1", Color.white);
            game.pieces.addPiece(queen);
            expect(queen.toString()).toBe("white Qu")
        });
        test('Black queen to string is black Qu', function () {
            let queen= new Queen("A1", Color.black);
            game.pieces.addPiece(queen);
            expect(queen.toString()).toBe("black Qu")
        });
    });
});
