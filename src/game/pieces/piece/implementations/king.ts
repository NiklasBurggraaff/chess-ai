import {Direction} from "../../../../utils/position";
import {SimplePiece} from "../simplePiece";

/**
 * Class containing information about the king piece
 */
// TODO: Try to fix this branch not covered rather than ignore it
/* istanbul ignore next */
export class King extends SimplePiece {
    /**
     * The number of points that the piece is worth
     */
    piecePoints: number = 1000;

    /**
     * The initials of the piece to be put on the piece
     */
    initials: string = "Ki";

    /**
     * The directions of the lines that the piece can move
     */
    stepDirections: Direction[] = this.straightStepDirections.concat(this.diagonalStepDirections);

    /**
     * The maximum number of steps that the piece can moves
     */
    maxSteps: number = 1;
}
