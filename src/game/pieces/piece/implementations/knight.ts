import {Direction} from "../../../../utils/position";
import {SimplePiece} from "../simplePiece";

/**
 * Class containing information about the knight piece
 */
// TODO: Try to fix this branch not covered rather than ignore it
/* istanbul ignore next */
export class Knight extends SimplePiece {
    /**
     * The number of points that the piece is worth
     */
    piecePoints: number = 3;

    /**
     * The initials of the piece to be put on the piece
     */
    initials: string = "Kn";

    /**
     * The directions of the lines that the piece can move
     */
    stepDirections: Direction[] = this.knightStepDirections;

    /**
     * The maximum number of steps that the piece can moves
     */
    maxSteps: number = 1;
}
