import {Color} from "../piece";
import {Knight} from "./knight";
import {Game} from "../../../game";
import {Pawn} from "./pawn";
import {PositionsComparator} from "../positionsComparator"

describe("Knight class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White knight is worth 3 points', function () {
            let knight = new Knight("A1", Color.white);
            game.pieces.addPiece(knight);
            expect(knight.points()).toBe(3);
        });
        test('Black knight is worth -3 points', function () {
            let knight = new Knight("A1", Color.black);
            expect(knight.points()).toBe(-3);
        });
    });

    describe('Possible move positions', function () {
        test('White knight on B1 can jump to 4 squares around it', function () {
            let knight = new Knight("B1", Color.white);
            game.pieces.addPiece(knight);
            let possibleMoves = knight.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "C3", "D2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black knight on B8 can jump to 4 squares around it', function () {
            let knight = new Knight("B8", Color.black);
            game.pieces.addPiece(knight);
            let possibleMoves = knight.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A6", "C6", "D7"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White knight on D4 can jump all 8 directions', function () {
            let knight = new Knight("D4", Color.white);
            game.pieces.addPiece(knight);
            let possibleMoves = knight.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["C2", "B3", "B5", "C6", "E6", "F5", "F3", "E2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White knight on B1 can jump over a white pawn on B2 to 3 squares around it', function () {
            let pawn = new Pawn("B2", Color.white);
            game.pieces.addPiece(pawn);

            let knight = new Knight("B1", Color.white);
            game.pieces.addPiece(knight);
            let possibleMoves = knight.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "C3", "D2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White knight on B1 can NOT jump to a white pawn on A3', function () {
            let pawn = new Pawn("A3", Color.white);
            game.pieces.addPiece(pawn);

            let knight = new Knight("B1", Color.white);
            game.pieces.addPiece(knight);
            let possibleMoves = knight.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["C3", "D2"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

    });

    describe('To string', function () {
        test('White knight to string is white Kn', function () {
            let knight = new Knight("A1", Color.white);
            game.pieces.addPiece(knight);
            expect(knight.toString()).toBe("white Kn")
        });
        test('Black knight to string is black Kn', function () {
            let knight= new Knight("A1", Color.black);
            game.pieces.addPiece(knight);
            expect(knight.toString()).toBe("black Kn")
        });
    });
});
