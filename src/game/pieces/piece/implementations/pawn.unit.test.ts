import {Color} from "../piece";
import {Pawn} from "./pawn";
import {Game} from "../../../game";
import {PositionsComparator} from "../positionsComparator"

describe("Pawn class", function () {
    let game = new Game();

    beforeEach(function () {
        game.resetGame();
    });

    describe('Points', function () {
        test('White pawn is worth 1 point', function () {
            let pawn = new Pawn("A1", Color.white);
            game.pieces.addPiece(pawn);
            expect(pawn.points()).toBe(1);
        });
        test('Black pawn is worth -1 points', function () {
            let pawn = new Pawn("A1", Color.black);
            game.pieces.addPiece(pawn);
            expect(pawn.points()).toBe(-1);
        });
    });

    describe('Possible move positions', function () {
        test('White pawn on A2 can make single and double step', function () {
            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "A4"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black pawn on A7 can make single and double step', function () {
            let pawn = new Pawn("A7", Color.black);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A6", "A5"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A3 that has already moved can only make single step', function () {
            let pawn = new Pawn("A3", Color.white, true);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A4"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A7 can only make single step', function () {
            let pawn = new Pawn("A7", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A8"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('Black pawn A6 that has already moved can only make single step', function () {
            let pawn = new Pawn("A6", Color.black, true);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A5"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A8 cannot move', function () {
            let pawn = new Pawn("A8", Color.white, true);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });

        test('White pawn on A2 with a white pawn on A3 cannot move', function () {
            let pawn2 = new Pawn("A3", Color.white);
            game.pieces.addPiece(pawn2);

            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A2 with a black pawn on A3 cannot move', function () {
            let pawn2 = new Pawn("A3", Color.black);
            game.pieces.addPiece(pawn2);

            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = [];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A2 with a white pawn on B3 can NOT capture it', function () {
            let pawn2 = new Pawn("B3", Color.white);
            game.pieces.addPiece(pawn2);

            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "A4"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
        test('White pawn on A2 with a black pawn on B3 can capture it', function () {
            let pawn2 = new Pawn("B3", Color.black);
            game.pieces.addPiece(pawn2);

            let pawn = new Pawn("A2", Color.white);
            game.pieces.addPiece(pawn);
            let possibleMoves = pawn.possibleMoves(game.pieces);
            let expectedMoveDestinations: string[] = ["A3", "A4", "B3"];

            PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations);
        });
    });

    describe('To string', function () {
        test('White pawn to string is white Pa', function () {
            let pawn = new Pawn("A1", Color.white);
            game.pieces.addPiece(pawn);
            expect(pawn.toString()).toBe("white Pa")
        });
        test('Black pawn to string is black Pa', function () {
            let pawn = new Pawn("A1", Color.black);
            game.pieces.addPiece(pawn);
            expect(pawn.toString()).toBe("black Pa")
        });
    });
});
