import {Position} from "../../../utils/position";
import {Move} from "../../move";

/**
 * Class to compare positions
 */
export class PositionsComparator {
    /**
     * Compares the given moves to the expectedMoveDestinations
     * Expects them to contain the same values without regard to the order
     * Fails the test if they are not
     *
     * @param moves - The given moves to compare
     * @param expectedMoveDestinations - The expected move destination positions of the given moves
     */
    static comparePositions(moves: Move[], expectedMoveDestinations: Position[] | string[]) {
        let moveDestinationIds: String[] = moves.map((move: Move) => {
            return move.destinationPosition.squareId;
        });

        if (expectedMoveDestinations.length > 0) {
            if (expectedMoveDestinations[0] instanceof Position) {
                expectedMoveDestinations = expectedMoveDestinations as Position[];
                expectedMoveDestinations = expectedMoveDestinations.map((position: Position) => {
                    return position.squareId;
                });
            }
        }

        let expectedMovesDestinationIds = expectedMoveDestinations as String[];

        for (let i = 0; i < expectedMovesDestinationIds.length; i++) {
            let expectedMoveDestinationId = expectedMovesDestinationIds[i];
            expect(moveDestinationIds).toContain(expectedMoveDestinationId);
        }

        for (let i = 0; i < moveDestinationIds.length; i++) {
            let moveDestinationId = moveDestinationIds[i];
            expect(expectedMovesDestinationIds).toContain(moveDestinationId);
        }

        expect(moves.length).toBe(expectedMoveDestinations.length);
    }
}
