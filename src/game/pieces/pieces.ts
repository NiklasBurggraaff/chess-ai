import {MaybePiece, Piece} from "./piece/piece";
import {Position} from "../../utils/position";
import {Game} from "../game";
import {Move} from "../move";

export class Pieces {
    /**
     * The pieces at for each position
     * pieces[file][rank]
     */
    private pieces: MaybePiece[][] = [];

    /**
     * @returns The a copy of the pieces of the game
     */
    getPieces(): MaybePiece[][] {
        return Array.from(this.pieces);
    }

    /**
     * Get the piece at the given position
     * @param position - The position to get the piece from
     * @returns The piece at the given position
     */
    getPiece(position: Position): MaybePiece {
        if (!position.isValid()) {
            throw new Error("The position to get the piece from is invalid.")
        }

        return this.pieces[position.file][position.rank];
    }

    /**
     * Piece class constructor
     * Reset the pieces array to make it valid
     */
    constructor() {
        this.resetPieces();
    }

    /**
     * Adds a piece to the pieces matrix
     * @param piece - The piece to add
     */
    addPiece(piece: Piece) {
        let position: Position = piece.getPosition();

        if (this.pieces[position.file][position.rank] == null) {
            this.pieces[position.file][position.rank] = piece;
        } else {
            throw new Error("A piece already exists at the position of the given piece to be added.")
        }
    }

    /**
     * Removes a piece at the given position from the pieces matrix
     * @param piece - The piece to remove
     */
    removePiece(piece: Piece) {
        let position = piece.getPosition();
        let pieceAtPosition = this.getPiece(position);

        if (!piece.isEqualTo(pieceAtPosition)) {
            throw new Error("The piece to remove is not equal to the piece at its position.");
        }

        this.pieces[position.file][position.rank] = null;
    }

    /**
     * Removes a piece at the given position from the pieces matrix
     * @param position - The position to remove a piece from
     */
    removePieceAtPosition(position: Position) {
        if (this.pieces[position.file][position.rank] != null) {
            this.pieces[position.file][position.rank] = null;
        } else {
            throw new Error("The given position to remove a piece from does not have a piece to be removed.")
        }
    }

    /**
     * Move a piece according to the given move
     * Removes the piece to be captured and moves the piece to the destination position
     *
     * @param move - The move that a piece needs to make
     */
    makeMove(move: Move) {
        let capturedPiece: MaybePiece = move.pieceCaptured;

        if (capturedPiece != null) {
            this.removePiece(capturedPiece as Piece);
        }

        let piece = this.getPiece(move.departurePosition);

        if (piece == null) {
            throw new Error("There is no piece at the departure position of the piece.");
        }

        this.removePiece(piece);

        piece.move(move);

        this.addPiece(piece);
    }

    /**
     * Reset the pieces
     */
    resetPieces() {
        this.pieces = [];
        for (let i = 0; i < Game.boardSize; i++) {
            this.pieces.push([]);
            for (let j = 0; j < Game.boardSize; j++) {
                this.pieces[i].push(null);
            }
        }
    }
}
