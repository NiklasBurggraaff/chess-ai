import {Pawn} from "./piece/implementations/pawn";
import {Color, MaybePiece} from "./piece/piece";
import {Position} from "../../utils/position";
import {Pieces} from "./pieces";
import {Rook} from "./piece/implementations/rook";
import {Move} from "../move";

describe('Pieces class', function () {
    let pieces = new Pieces();

    beforeEach(function () {
        pieces = new Pieces();
    });

    describe('Add piece', function () {
        test('Adding a piece on A1 adds it to the pieces', function () {
            let piece = new Rook("A1", Color.white);
            pieces.addPiece(piece);
            expect(pieces.getPiece(Position.atSquareId("A1"))).toStrictEqual(piece);
        });
        test('Adding a piece on A1 after already added a piece there throws an error', function () {
            let rook = new Rook("A1", Color.white);
            pieces.addPiece(rook);
            let pawn = new Pawn("A1", Color.white);
            expect(function () {
                pieces.addPiece(pawn);
            }).toThrow("A piece already exists at the position of the given piece to be added.");
        });
    });

    describe('Remove piece', function () {
        test('Removing a piece after adding it there makes the piece at its position null', function () {
            let position = Position.atSquareId("A1");

            let piece = new Rook(position, Color.white);
            pieces.addPiece(piece);
            pieces.removePiece(piece);

            expect(pieces.getPiece(position)).toBeNull();
        });
        test('Removing a piece that has not been added throws an error', function () {
            let piece = new Rook("A1", Color.white);

            expect(function () {
                pieces.removePiece(piece);
            }).toThrow("The piece to remove is not equal to the piece at its position.");
        });
    });

    describe('Remove piece at position', function () {
        test('Removing a piece on A1 after adding a piece there makes the piece at that position null', function () {
            let position = Position.atSquareId("A1");

            let piece = new Rook(position, Color.white);
            pieces.addPiece(piece);

            pieces.removePieceAtPosition(position);

            expect(pieces.getPiece(position)).toBeNull();
        });
        test('Removing a piece on A1 throws an error there is not piece there', function () {
            let position = Position.atSquareId("A1");
            expect(function () {
                pieces.removePieceAtPosition(position);
            }).toThrow("The given position to remove a piece from does not have a piece to be removed.");
        });
    });

    describe('Get pieces', function () {
        test('Pieces in initialised Pieces object is an 8x8 matrix of null', function () {
            let expectedPieces: MaybePiece[][] = [];
            for (let file = 0; file < 8; file++) {
                expectedPieces.push([]);
                for (let rank = 0; rank < 8; rank++) {
                    expectedPieces[file].push(null);
                }
            }

            expect(pieces.getPieces()).toStrictEqual(expectedPieces);
        });
    });

    describe('Get piece', function () {
        test('Piece on A1 is null', function () {
            let position = Position.atSquareId("A1");
            expect(pieces.getPiece(position)).toBeNull();
        });
        test('Piece on A1 is a white rook after adding it there', function () {
            let position = Position.atSquareId("A1");

            let rook = new Rook(position, Color.white);
            pieces.addPiece(rook);

            let expectedString = rook.toString();
            expect(pieces.getPiece(position)!.toString()).toBe(expectedString);
        });
        test('Using invalid position with file 0 and rank -1 throws an error', function () {
            let position = new Position(0, -1);
            expect(function () {
                pieces.getPiece(position)
            }).toThrow("The position to get the piece from is invalid.");
        });
    });

    describe('Make move', function () {
        test('The position of a piece after a move is made is the destination position', function () {
            let departurePosition = Position.atSquareId("A2");
            let destinationPosition = Position.atSquareId("A3");

            let pawn = new Pawn(departurePosition, Color.white);
            pieces.addPiece(pawn);
            let move = new Move(departurePosition, destinationPosition);

            expect(pieces.getPiece(departurePosition)!.isEqualTo(pawn)).toBeTruthy();

            pieces.makeMove(move);

            expect(pieces.getPiece(destinationPosition)!.isEqualTo(pawn)).toBeTruthy();
        });
        test('The captured piece is removed', function () {
            let departurePosition = Position.atSquareId("A1");
            let destinationPosition = Position.atSquareId("A7");

            let pawn = new Pawn(destinationPosition, Color.black);
            pieces.addPiece(pawn);
            let rook = new Pawn(departurePosition, Color.white);
            pieces.addPiece(rook);

            let move = new Move(departurePosition, destinationPosition, pawn, true);

            expect(pieces.getPiece(departurePosition)!.isEqualTo(rook)).toBeTruthy();
            expect(pieces.getPiece(destinationPosition)!.isEqualTo(pawn)).toBeTruthy();

            pieces.makeMove(move);

            expect(pieces.getPiece(destinationPosition)!.isEqualTo(rook)).toBeTruthy();
        });
        test('Making a move with no piece at the departure position throws an error', function () {
            let departurePosition = Position.atSquareId("A2");
            let destinationPosition = Position.atSquareId("A3");

            let move = new Move(departurePosition, destinationPosition);

            expect(function () {
                pieces.makeMove(move);
            }).toThrow("There is no piece at the departure position of the piece.");
        });
    });
});
