import {Move} from "./move";
import {Position} from "../utils/position";

describe('Move class', function () {
    test('Initialise move', function () {
        let move = new Move(Position.atSquareId("A1"), Position.atSquareId("A2"));
        expect(typeof move).toBe("object");
    });
});
