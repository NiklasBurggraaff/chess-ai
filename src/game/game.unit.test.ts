import {Game} from "./game";
import {Direction, Position} from "../utils/position";
import {PositionsComparator} from "./pieces/piece/positionsComparator";

describe("Game class", function () {
    let game = new Game();

    beforeEach(function () {
        game = new Game();

        game.setupGame();
    });

    describe('Points', function () {
        test(' Points value of a setup game is 0', function () {
            expect(game.points()).toBe(0);
        });
        test('Points value of a reset game is 0', function () {
            game.resetGame();
            expect(game.points()).toBe(0);
        });
    });

    describe('Setup game', function () {
        test('Pieces of a setup game are only on ranks 0 1 6 and 7', function () {
            let pieces = game.pieces.getPieces();

            for (let file = 0; file < 8; file++) {
                for (let rank = 0; rank < 8; rank++) {
                    if (rank == 0 || rank == 1 || rank == 6 || rank == 7) {
                        expect(pieces[file][rank]).not.toBeNull();
                    } else {
                        expect(pieces[file][rank]).toBeNull();
                    }
                }
            }
        });

        describe('Possible move positions of initial pieces', function () {
            test("White pawns can make single and double step", function () {
                for (let file = 0; file < Game.boardSize; file++) {
                    let position = new Position(file, 1);

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: Position[] = [
                        position.getMovedPosition(new Direction(0, 1)),
                        position.getMovedPosition(new Direction(0, 2))
                    ];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                }
            });

            test("Black pawns can make single and double step", function () {
                for (let file = 0; file < Game.boardSize; file++) {
                    let position = new Position(file, 6);

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: Position[] = [
                        position.getMovedPosition(new Direction(0, -1)),
                        position.getMovedPosition(new Direction(0, -2))
                    ];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                }
            });

            describe('Knights can jump over pawns', function () {
                test('White B1 knight', function () {
                    let position = Position.atSquareId("B1");

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: string[] = ["A3", "C3"];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                });
                test('White G1 knight', function () {
                    let position = Position.atSquareId("G1");

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: string[] = ["F3", "H3"];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                });
                test('Black B8 knight', function () {
                    let position = Position.atSquareId("B8");

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: string[] = ["A6", "C6"];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                });
                test('Black G8 knight', function () {
                    let position = Position.atSquareId("G8");

                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    let expectedMoveDestinations: string[] = ["F6", "H6"];

                    PositionsComparator.comparePositions(possibleMoves, expectedMoveDestinations)
                });
            });

            test('King queen bishop and rook can\'t move', function () {
                let positions: Position[] = [
                    new Position(0, 0),
                    new Position(2, 0),
                    new Position(3, 0),
                    new Position(4, 0),
                    new Position(5, 0),
                    new Position(7, 0),
                    new Position(0, 7),
                    new Position(2, 7),
                    new Position(3, 7),
                    new Position(4, 7),
                    new Position(5, 7),
                    new Position(7, 7),
                ];

                positions.forEach(function (position) {
                    let possibleMoves = game.pieces.getPiece(position)!.possibleMoves(game.pieces);

                    expect(possibleMoves.length).toBe(0);
                });
            });
        });
    });

    describe('Reset game', function () {
        test('Moves array is empty after resetting a game', function () {
            expect(game.getMoves().length).toBe(0);
        });
    });

    describe('Get moves', function () {
        test('Moves of setup game is empty', function () {
            expect(game.getMoves()).toStrictEqual([]);
        });
        test('Moves of reset game is empty', function () {
            game.resetGame();
            expect(game.getMoves()).toStrictEqual([]);
        });
    });

    describe('Square click', function () {
        let consoleOutput: string[] = [];
        const originalLog = console.log;

        // Replaces the console.log function with a function that adds the log to the console output array
        beforeEach(function () {
            console.log = (output: string) => consoleOutput.push(output);
        });

        // Resets the console output and the console.log function
        afterEach(function () {
            consoleOutput = [];
            console.log = originalLog;
        });

        test('Clicking the A3 square logs the square ID A3 and the piece null', function () {
            let position = Position.atSquareId("A3");
            let expectedLog = ["A3: null"];

            game.squareClick(position);
            expect(consoleOutput).toStrictEqual(expectedLog);
        });

        test('Clicking the A1 square logs the square ID A1, the piece white rook ' +
            'and the possible move positions which is empty', function () {
            let position = Position.atSquareId("A1");
            let moveDestinationPositions: Position[] = [];

            let expectedLog = [
                "A1: white Ro",
                moveDestinationPositions
            ];

            game.squareClick(position);
            expect(consoleOutput).toStrictEqual(expectedLog);
        });

        test('Clicking the A2 square logs the square ID A2, the piece white pawn ' +
            'and the possible move positions which are a single and double step', function () {
            let position = Position.atSquareId("A2");
            let moveDestinationPositions: Position[] = [
                Position.atSquareId("A3"),
                Position.atSquareId("A4")
            ];

            let expectedLog = [
                "A2: white Pa",
                moveDestinationPositions
            ];

            game.squareClick(position);
            expect(consoleOutput).toStrictEqual(expectedLog);
        });
    });
});
