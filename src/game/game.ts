import {Color, MaybePiece, Piece} from "./pieces/piece/piece";
import {Pawn} from "./pieces/piece/implementations/pawn";
import {Move} from "./move";
import {Position} from "../utils/position";
import {Rook} from "./pieces/piece/implementations/rook";
import {Knight} from "./pieces/piece/implementations/knight";
import {Bishop} from "./pieces/piece/implementations/bishop";
import {Queen} from "./pieces/piece/implementations/queen";
import {King} from "./pieces/piece/implementations/king";
import {Pieces} from "./pieces/pieces";

/**
 * Class containing the current state of the chess game and methods to change that state
 */
export class Game {

    /**
     * The number of squares in each rank and file
     */
    private static _boardSize: number = 8;

    /**
     * @returns The board size
     */
    static get boardSize(): number {
        return this._boardSize;
    }

    /**
     * @param value - The value to set the board size to
     */
    static set boardSize(value: number) {
        this._boardSize = value;
        Position.boardSize = value;
    }

    /**
     * The pieces in the game
     */
    public pieces: Pieces = new Pieces();

    /**
     * The moves played in the game
     */
    private moves: Move[] = [];

    /**
     * @returns The moves played in the game
     */
    getMoves(): Move[] {
        return this.moves;
    }

    /**
     * Sets up the game by adding the pieces at the start of a chess game
     */
    setupGame() {
        this.resetGame();

        for (let file = 0; file < Game.boardSize; file++) {
            this.pieces.addPiece(new Pawn(new Position(file, 1), Color.white));
            this.pieces.addPiece(new Pawn(new Position(file, 6), Color.black));
        }

        this.pieces.addPiece(new Rook("A1", Color.white));
        this.pieces.addPiece(new Rook("H1", Color.white));
        this.pieces.addPiece(new Rook("A8", Color.black));
        this.pieces.addPiece(new Rook("H8", Color.black));

        this.pieces.addPiece(new Knight("B1", Color.white));
        this.pieces.addPiece(new Knight("G1", Color.white));
        this.pieces.addPiece(new Knight("B8", Color.black));
        this.pieces.addPiece(new Knight("G8", Color.black));

        this.pieces.addPiece(new Bishop("C1", Color.white));
        this.pieces.addPiece(new Bishop("F1", Color.white));
        this.pieces.addPiece(new Bishop("C8", Color.black));
        this.pieces.addPiece(new Bishop("F8", Color.black));

        this.pieces.addPiece(new Queen("D1", Color.white));
        this.pieces.addPiece(new Queen("D8", Color.black));

        this.pieces.addPiece(new King("E1", Color.white));
        this.pieces.addPiece(new King("E8", Color.black));
    }

    /**
     * Reset the game
     */
    resetGame() {
        this.pieces.resetPieces();

        this.moves = [];
    }

    /**
     * @returns The points value of the board
     */
    points(): number {
        let points: number = 0;

        let piecesArray: MaybePiece[][] = this.pieces.getPieces();

        piecesArray.forEach(function (pieces) {
            pieces.forEach(function (piece) {
                if (piece != null) {
                    piece = piece as Piece;
                    points += piece.points();
                }
            })
        });

        return points;
    }

    /**
     * Function called when a square is clicked
     * @param position - The position of the square clicked
     */
    squareClick(position: Position) {
        let piece: MaybePiece = this.pieces.getPiece(position);
        let pieceString: string = piece == null ? "null" : piece.toString();
        console.log(position.squareId + ": " + pieceString);

        if (piece != null) {
            let possibleMoves = piece.possibleMoves(this.pieces);
            let possibleDestinationPositions = possibleMoves.map((move: Move) => {
                return move.destinationPosition
            });
            console.log(possibleDestinationPositions);
        }
    }
}
