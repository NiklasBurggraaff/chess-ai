import {Board, FileLabel, RankLabel, Square} from "./board";

describe('Board class', function () {
    describe('Get squares', function () {
        test('From the white player view the first (top left) square is A8', function () {
            let board = new Board(true);
            let squares: Square[] = board.getSquares();
            let firstSquare: Square = squares[0];
            expect(firstSquare.position.squareId).toBe("A8");
        });
        test('From the black player view the first (top left) square is H1', function () {
            let board = new Board(false);
            let squares: Square[] = board.getSquares();
            let firstSquare: Square = squares[0];
            expect(firstSquare.position.squareId).toBe("H1");
        });

        test('From the white player view the last (bottom right) square is a light square', function () {
            let board = new Board(true);
            let squares: Square[] = board.getSquares();
            let lastSquare: Square = squares[squares.length - 1];
            expect(lastSquare.color).toBe("light");
        });
        test('From the black player view the last (bottom right) square is a light square', function () {
            let board = new Board(false);
            let squares: Square[] = board.getSquares();
            let lastSquare: Square = squares[squares.length - 1];
            expect(lastSquare.color).toBe("light");
        });
    });

    describe('Get file labels', function () {
        test('From the white player view the first (left) file is the A file', function () {
            let board = new Board(true);
            let fileLabels: FileLabel[] = board.getFileLabels();
            let firstFileLabel: FileLabel = fileLabels[0];
            expect(firstFileLabel.file).toBe(0);
        });
        test('From the black player view the first (left) file is the H file', function () {
            let board = new Board(false);
            let fileLabels: FileLabel[] = board.getFileLabels();
            let firstFileLabel: FileLabel = fileLabels[0];
            expect(firstFileLabel.file).toBe(7);
        });
    });

    describe('Get rank labels', function () {
        test('From the white player view the last (bottom) rank is the 1st rank', function () {
            let board = new Board(true);
            let rankLabels: RankLabel[] = board.getRankLabels();
            let firstRankLabel: RankLabel = rankLabels[rankLabels.length - 1];
            expect(firstRankLabel.rank).toBe(0);
        });
        test('From the black player view the last (bottom) rank is the 8th rank', function () {
            let board = new Board(false);
            let rankLabels: RankLabel[] = board.getRankLabels();
            let firstRankLabel: RankLabel = rankLabels[rankLabels.length - 1];
            expect(firstRankLabel.rank).toBe(7);
        });
    });
});

describe("Square class", function () {
    describe('Color', function () {
        test("Square at file 0 and rank 0 is a dark square", function () {
            let square = new Square(0, 0);
            expect(square.color).toBe("dark");
        });
        test("Square at file 7 and rank 7is a dark square", function () {
            let square = new Square(7, 7);
            expect(square.color).toBe("dark");
        });

        test("Square at file 1 and rank 0 is a light square", function () {
            let square = new Square(1, 0);
            expect(square.color).toBe("light");
        });
        test("Square at file 6 and rank 7 is a light square", function () {
            let square = new Square(6, 7);
            expect(square.color).toBe("light");
        });
    });

    describe('Get HTML', function () {
        test("HTML object of square at file A and rank 1 has dark class and A1 id", function () {
            let square = new Square(0, 0);
            let color: string = "dark";
            let id: string = "A1";
            expect(square.getHTML()).toBe(`<div class=\"square ${color}\" id=\"${id}\"></div>`);
        });
    });
});

describe("FileLabel class", function () {
    describe('Get HTML', function () {
        test("HTML object of file label at file 0 has the correct A text", function () {
            let fileLabel = new FileLabel(0);
            let fileChar = "A";
            expect(fileLabel.getHTML()).toBe(`<div class="file-label">${fileChar}</div>`)
        });
        test("HTML object of file label at file 7 has the correct H text", function () {
            let fileLabel = new FileLabel(7);
            let fileChar = "H";
            expect(fileLabel.getHTML()).toBe(`<div class="file-label">${fileChar}</div>`)
        });
    });
});

describe("RankLabel class", function () {
    describe('Get HTML', function () {
        test("HTML object of rank label at rank 0 has the correct 1 text", function () {
            let rankLabel = new RankLabel(0);
            let rankNumber = 1;
            expect(rankLabel.getHTML()).toBe(`<div class="rank-label">${rankNumber}</div>`)
        });
        test("HTML object of rank label at rank 7 has the correct 8 text", function () {
            let rankLabel = new RankLabel(7);
            let rankNumber = 8;
            expect(rankLabel.getHTML()).toBe(`<div class="rank-label">${rankNumber}</div>`)
        });
    });
});
