import {Position} from "../utils/position";
import {Game} from "../game/game";

/**
 * A class with methods to get the elements for the board
 */
export class Board {
    /**
     * True if the view of the board should be from the white player
     * False if the view of the board should be from the black player
     */
    private isWhitePlayerView: boolean;

    /**
     * @param isWhitePlayerView - Whether the view of the board should be of the white player
     */
    constructor(isWhitePlayerView: boolean) {
        this.isWhitePlayerView = isWhitePlayerView;
    }

    /**
     * @returns The squares that need to be put onto the squares of the board
     */
    getSquares(): Square[] {
        let squares: Square[] = [];

        for (let i: number = 0; i < Game.boardSize; i++) {
            let rank: number = this.rankIndexFromDisplayIndex(i);

            for (let j: number = 0; j < Game.boardSize; j++) {
                let file: number = this.fileIndexFromDisplayIndex(j);

                let square = new Square(file, rank);
                squares.push(square);
            }
        }

        return squares;
    }

    /**
     * @returns The file labels that need to be put onto the file labels of the board
     */
    getFileLabels(): FileLabel[] {
        let fileLabels: FileLabel[] = [];

        for (let i: number = 0; i < Game.boardSize; i++) {
            let file: number = this.fileIndexFromDisplayIndex(i);

            let fileLabel = new FileLabel(file);
            fileLabels.push(fileLabel);
        }

        return fileLabels
    }

    /**
     * @returns The rank labels that need to be put onto the rank labels of the board
     */
    getRankLabels(): RankLabel[] {
        let rankLabels: RankLabel[] = [];

        for (let i: number = 0; i < Game.boardSize; i++) {
            let rank: number = this.rankIndexFromDisplayIndex(i);

            let rankLabel = new RankLabel(rank);
            rankLabels.push(rankLabel);
        }

        return rankLabels;
    }

    /**
     * @param orderIndex - The index of the rank in the order it appears on the board
     * @returns The rank number of the rank at the given order index
     */
    fileIndexFromDisplayIndex(orderIndex: number): number {
        if (this.isWhitePlayerView) {
            return orderIndex;
        } else {
            let maxIndex: number = Game.boardSize - 1;
            return maxIndex - orderIndex;
        }
    }

    /**
     * @param orderIndex - The index of the rank in the order it appears on the board
     * @returns The rank number of the rank at the given order index
     */
    rankIndexFromDisplayIndex(orderIndex: number): number {
        if (this.isWhitePlayerView) {
            let maxIndex: number = Game.boardSize - 1;
            return maxIndex - orderIndex;
        } else {
            return orderIndex;
        }
    }
}

/**
 * A square ui element to be added to the squares of the board
 */
export class Square {
    /**
     * The position of the square
     */
    position: Position;
    /**
     * True if it is a light square, and false if it is a dark square
     */
    private readonly isLight: boolean;

    /**
     * @param file - The file of the square
     * @param rank - The rank of the square
     */
    constructor(file: number, rank: number) {
        this.position = new Position(file, rank);

        let sum = rank + file;
        this.isLight = sum % 2 == 1;
    }

    /**
     * @returns The color of the square (light | dark)
     */
    get color(): string {
        if (this.isLight) {
            return "light";
        } else {
            return "dark";
        }
    }

    /**
     * @returns HTML of the square
     */
    getHTML(): string {
        return `<div class="square ${this.color}" id="${this.position.squareId}"></div>`;
    }
}

/**
 * A file label UI element to be added to the file labels of the board
 */
export class FileLabel {
    /**
     * The file of the label
     */
    file: number;

    /**
     * @param file - The file of the label
     */
    constructor(file: number) {
        this.file = file;
    }

    /**
     * @returns The character index of the file of the label
     */
    get fileChar(): string {
        let firstFileChar = 'A'.charCodeAt(0);
        return String.fromCharCode(firstFileChar + this.file);
    }

    /**
     * @returns HTML of the file label
     */
    getHTML(): string {
        return `<div class="file-label">${this.fileChar}</div>`;
    }
}

/**
 * A rank label UI element to be added to the rank labels of the board
 */
export class RankLabel {
    /**
     * The rank of txhe label
     */
    rank: number;

    /**
     * @param rank - The rank of the label
     */
    constructor(rank: number) {
        this.rank = rank;
    }

    /**
     * @returns The number of the rank of the label
     */
    get rankNumber(): number {
        return this.rank + 1;
    }

    /**
     * @returns HTML of the rank label
     */
    getHTML(): string {
        return `<div class="rank-label">${this.rankNumber}</div>`;
    }
}
