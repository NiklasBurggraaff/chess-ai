import {Direction, Position} from "./position";
import {Game} from "../game/game";

describe("Position class", function () {
    describe('Square ID', function () {
        test("Position at file 0 and rank 0 has square ID of A1", function () {
            let position = new Position(0, 0);
            expect(position.squareId).toBe("A1");
        });
        test("Position at rank 7 and file 7 has square ID of H8", function () {
            let position = new Position(7, 7);
            expect(position.squareId).toBe("H8");
        });
    });

    describe('Is valid', function () {
        test('Position at file 0 and rank 0 is valid', function () {
            let position = new Position(0, 0);
            expect(position.isValid()).toBeTruthy();
        });
        test('Position at file 7 and rank 7 is valid', function () {
            let position = new Position(7, 7);
            expect(position.isValid()).toBeTruthy();
        });
        test('Position at file 0 and rank -1 is NOT valid', function () {
            let position = new Position(0, -1);
            expect(position.isValid()).toBeFalsy();
        });
        test('Position at file -1 and rank 0 is NOT valid', function () {
            let position = new Position(-1, 0);
            expect(position.isValid()).toBeFalsy();
        });
        test('Position at file 0 and rank 7 with a board size of 7 is NOT valid', function () {
            Game.boardSize = 7;
            let position = new Position(0, 7);
            expect(position.isValid()).toBeFalsy();
            Game.boardSize = 8;
        });
        test('Position at file 7 and rank 0 with a board size of 7 is NOT valid', function () {
            Game.boardSize = 7;
            let position = new Position(7, 0);
            expect(position.isValid()).toBeFalsy();
            Game.boardSize = 8;
        });
    });

    describe('Copy', function () {
        test('Copy of position at file 0 and rank 0 has the same square ID', function () {
            let position = new Position(0, 0);
            let positionCopy = position.copy();
            expect(position.squareId).toBe(positionCopy.squareId);
        });
        test('Copy of position at file 0 and rank 0 has the same square ID', function () {
            let position = new Position(7, 7);
            let positionCopy = position.copy();
            expect(position.squareId).toBe(positionCopy.squareId);
        });
        test('Moving the copy of a position does not change the original position', function () {
            let position = new Position(0, 0);
            let squareId = position.squareId;

            let positionCopy = position.copy();
            positionCopy.move(new Direction(1, 1));

            expect(position.squareId).toBe(squareId);
        });
        test('Moving a position does not change the position of a copy of the original position', function () {
            let position = new Position(0, 0);

            let positionCopy = position.copy();
            let squareId = positionCopy.squareId;

            position.move(new Direction(1, 1));

            expect(positionCopy.squareId).toBe(squareId);
        });
    });

    describe('Get moved position', function () {
        test('Position at file 0 and rank 0 moved by 1 file and 1 rank has a squareID of B2', function () {
            let position = new Position(0, 0);
            let positionCopy = position.getMovedPosition( new Direction(1, 1));
            expect(positionCopy.squareId).toBe("B2");
        });
        test('Position at file 7 and rank 7 moved by -1 file and -1 rank has a squareID of G7', function () {
            let position = new Position(7, 7);
            let positionCopy = position.getMovedPosition(new Direction(-1, -1));
            expect(positionCopy.squareId).toBe("G7");
        });
    });

    describe('Position at square ID', function () {
        test('Position at square ID A1 has square ID A1', function () {
            let position: Position = Position.atSquareId("A1");
            expect(position.squareId).toBe("A1");
        });
        test('Position at square ID H8 has square ID H8', function () {
            let position: Position = Position.atSquareId("H8");
            expect(position.squareId).toBe("H8");
        });
        test('Position at empty square ID does not have length 2', function () {
            let squareId = "";
            expect(function () {
                Position.atSquareId(squareId);
            }).toThrow("The given squareId string \"" + squareId + "\" does not have length 2.");
        });
        test('Position at A9 is not valid', function () {
            let squareId = "A9";
            expect(function () {
                Position.atSquareId(squareId);
            }).toThrow("The given squareId string \"" + squareId + "\" is not valid.");
        });
        test('Position at I1 is not valid', function () {
            let squareId = "I1";
            expect(function () {
                Position.atSquareId(squareId);
            }).toThrow("The given squareId string \"" + squareId + "\" is not valid.");
        });
    });

    describe('Is equal to', function () {
        test('Positions at same square ID are equal', function () {
            let squareId = "A1";
            let position1 = Position.atSquareId(squareId);
            let position2 = Position.atSquareId(squareId);

            expect(position1.isEqualTo(position2)).toBeTruthy();
        });
        test('Positions on different files are NOT equal', function () {
            let position1 = new Position(0, 0);
            let position2 = new Position(1, 0);

            expect(position1.isEqualTo(position2)).toBeFalsy();
        });
        test('Positions on different ranks are NOT equal', function () {
            let position1 = new Position(0, 0);
            let position2 = new Position(0, 1);

            expect(position1.isEqualTo(position2)).toBeFalsy();
        });
    });
});
