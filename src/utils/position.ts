/**
 * The position of a piece
 */
export class Position {
    /**
     * Get the boardSize store in the Position class
     */
    static get boardSize(): number {
        return this._boardSize;
    }

    /**
     * Set the boardSize store in the Position class
     */
    static set boardSize(value: number) {
        this._boardSize = value;
    }

    /**
     * The number of squares in each rank and file
     */
    private static _boardSize: number = 8;

    /**
     * The index of the rank of the position
     */
    public rank: number;
    /**
     * The index of the file of the position
     */
    public file: number;

    /**
     * The char code of the first file
     */
    private static firstFileCharCode = 'A'.charCodeAt(0);

    /**
     * @param file - The file of the position
     * @param rank - The rank of the position
     */
    constructor(file: number, rank: number) {
        this.file = file;
        this.rank = rank;
    }

    /**
     * Parses the square ID in order to return a position at that square
     * @param squareId - The square ID of the position to be returned
     * @returns The position at the given square ID
     */
    static atSquareId(squareId: string): Position {
        if (squareId.length != 2) {
            throw new Error("The given squareId string \"" + squareId + "\" does not have length 2.");
        }

        let fileChar: string = squareId.charAt(0);
        let file = fileChar.charCodeAt(0) - this.firstFileCharCode;

        let rankNumber: number = parseInt(squareId.charAt(1));
        let rank = rankNumber - 1;

        let position = new Position(file, rank);

        if (!position.isValid()) {
            throw new Error("The given squareId string \"" + squareId + "\" is not valid.");
        }

        return position;
    }

    /**
     * Move the position by the given amounts
     * @param direction - The direction to move the piece in
     */
    move(direction: Direction) {
        this.file += direction.deltaFile;
        this.rank += direction.deltaRank;
    }

    /**
     * @returns A copy of the position
     */
    copy(): Position {
        return new Position(this.file, this.rank);
    }

    /**
     * Get a new position that has been moved by the given amounts
     * @param direction - The direction to move the position
     * @returns A new position that has been moved by the given amounts
     */
    getMovedPosition(direction: Direction): Position {
        let position = this.copy();
        position.move(direction);
        return position;
    }

    /**
     * @returns The character index of the file at the position
     */
    private get fileChar(): string {
        return String.fromCharCode(Position.firstFileCharCode + this.file);
    }

    /**
     * @returns The number of the rank at the position
     */
    private get rankNumber(): number {
        return this.rank + 1;
    }

    /**
     * @returns The ID of the square at the position
     */
    get squareId(): string {
        return `${this.fileChar}${this.rankNumber}`;
    }

    /**
     * Check if the position is valid based on the size of the board
     * @returns True if the position is valid, and false if the position is not valid
     */
    isValid(): boolean {
        if (this.file < 0 || this.file >= Position.boardSize) {
            // If the file is not too large or too small the position is not valid
            return false;
        }
        if (this.rank < 0 || this.rank >= Position.boardSize) {
            // If the rank is not too large or too small the position is not valid
            return false;
        }

        // If the rank and file are both valid return true
        return true;
    }

    /**
     * Checks if the given position is equal to this position
     * @param position - The position to compare this to
     * @returns True if the positions are equal, and false otherwise
     */
    isEqualTo(position: Position): boolean {
        return this.file == position.file && this.rank == position.rank;
    }
}

/**
 * A direction a piece can move in
 */
export class Direction {
    constructor(deltaFile: number, deltaRank: number) {
        this.deltaFile = deltaFile
        this.deltaRank = deltaRank;
    }

    public deltaFile: number;
    public deltaRank: number;
}
