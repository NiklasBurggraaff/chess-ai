import $ from "jquery";
import {Board, FileLabel, RankLabel, Square} from "./ui-elements/board";
import {Game} from "./game/game";
import {MaybePiece} from "./game/pieces/piece/piece";
import {Position} from "./utils/position";

/**
 * The game
 */
export let game: Game = new Game();

/**
 * Function called when the page in finished loading
 */
$(function() {
    setupBoard();
    setupGame();
});

/**
 * True if the view of the board should be of the white player
 * False if the view of the board should be from the black player
 */
let isWhitePlayerView: boolean = true;

/**
 * Generate the elements required for the board
 */
function setupBoard() {
    let board = new Board(isWhitePlayerView);

    let squares: Square[] = board.getSquares();
    squares.forEach(addSquare);

    let fileLabels: FileLabel[] = board.getFileLabels();
    fileLabels.forEach(addFileLabel);
    let rankLabels: RankLabel[] = board.getRankLabels();
    rankLabels.forEach(addRankLabel);
}

/**
 * Add a square square the squares of the board
 * @param square - The HTML of the square to be added
 */
function addSquare(square: Square) {
    $('#squares').append(square.getHTML());

    // Connect the game squareClick function to the onclick event of the square
    $(`#${square.position.squareId}`).click(function () {
        game.squareClick(square.position);
    });
}

/**
 * Add a file label to the file labels of the board
 * @param fileLabel - The HTML of the file labels to be added
 */
function addFileLabel(fileLabel: FileLabel) {
    $('#file-labels-top').append(fileLabel.getHTML());
    $('#file-labels-bottom').append(fileLabel.getHTML());
}

/**
 * Add a rank label to the rank labels of the board
 * @param rankLabel - The HTML of the file labels to be added
 */
function addRankLabel(rankLabel: RankLabel) {
    $('#rank-labels-left').append(rankLabel.getHTML());
    $('#rank-labels-right').append(rankLabel.getHTML());
}

/**
 * Setup the game by adding the pieces to the board
 */
function setupGame() {
    game.setupGame();
    addPieces()
}

/**
 * Add all the pieces to the board
 */
export function addPieces() {
    for (let file = 0; file < Game.boardSize; file++) {
        for (let rank = 0; rank < Game.boardSize; rank++) {
            let position = new Position(file, rank);
            addPiece(position);
        }
    }
}

/**
 * Add the piece to the board
 * @param position - The position of the piece to add
 */
function addPiece(position: Position) {
    let squareId: string = position.squareId;
    let piece: MaybePiece = game.pieces.getPiece(position);
    if (piece == null) {
        $("#" + squareId).html("");
    } else {
        $("#" + squareId).html(piece.getHTML());
    }
}
