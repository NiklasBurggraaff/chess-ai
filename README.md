# Chess AI

Chess AI is a project where I am creating a chess game from scratch,
and creating an AI to play it.

This README file explains this project, and how to run the chess AI.
The CHANGELOG file shows all the changes for each release.

## Goal

My main goal of this project develop software development skills.
So this does not only include programming, but more importantly the other
parts of the job such as creating tests, documenting, continuous integration,
and the general workflow

Here is a list of the specific skills I am learning through this project:
- TypeScript - The main language I am using
- Node JS - The game is run on a Node JS server
- Jest - I have tests to cover 100% of my code
- TypeDoc - I am properly documenting all of my code
- GitLab
    - CI/CD - I have a continuous integration pipeline to build, test and document all my code
    - Issues - My workflow is based on using this

## Installation

1. Install Node JS from the [download page](https://nodejs.org/en/download/)

1. Clone the repository into the directory you want it using the following command:

    ```shell script
    git clone git@gitlab.com:NiklasBurggraaff/chess-ai.git
    ```

1. Install all the npm dependencies using the following command:

    ```shell script
    npm install
    ```

## Usage 

### Building

Build the TypeScript project to generate JavaScript code using the following command:

```shell script
npm run-script build
```

The project will be built using the configuration in the `src/tsconfig.json` file.
The output will be in the `public/` directory
which the Node JS server provides access to.

### Running

1. Start the project's Node JS server using the following command:

    ```shell script
    npm start
    ```

1. Open the game using by opening the following link in your preferred browser:
    
    http://localhost:3000

### Testing

Test the project using the Jest unit tests using the following command:

```shell script
npm test
```

The project will be tested using the `src/jest.config.json` file.
This will run all of the tests that it can find,
and make sure there is a 100% code coverage.
A cobertura code coverage report is generated at `coverage/cobertura-coverage.xml`,
and a website to display the code coverage can be found at `coverage/lcov-report/index.html`

### Generating documentation

Generate documentation of the project with TypeDoc using the following command:

```shell script
npm run-script doc
```

The website with the documentation can be found at `doc/index.html`

## Coding workflow

1. Create an issue
1. Create a milestone for a release to solve a number of issues
1. Create a branch to solve each issue in the milestone
1. Merge branches to solve issues
1. Create a release when all issues in milestone are resolved

## License
[MIT](https://choosealicense.com/licenses/mit/)
