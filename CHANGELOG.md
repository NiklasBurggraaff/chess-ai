# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Functionality to move a piece

### Changed
- Replaced jQuery file with node dependency
- Renamed pieceTaken to pieceCaptured

## [0.1.1] - 2020-12-17

### Added
- Pieces class to contain information about the pieces in the game
- Piece constructor can take squareId of piece or Position object
- Function to construct Position object at a given squareId
- Functions added to compare positions and pieces
- Jest cache directory so that it can be deleted when it causes issues of not detecting changes

### Changed
- Terminology of column row replaced with file and rank respectively
- Functionality of Column and Row classes were moved to Position, FileLabel, and RankLabel classes
- Changed possibleMovePositions method in Piece class to possibleMoves,
  returning the moves rather than just the destination positions
- Improved descriptions of tests
- Merge request templates contain a checkbox for "Updated documentation" rather than "Updated README"
- Moved webmanifest.json file

### Removed
- Column and Row classes, alongside the abstract Line class

### Fixed
- Swapped positions of the knights and bishops to be on the correct positions on the board


## [0.1.0] - 2020-09-02

### Added
- [Node.js](https://nodejs.org/en/) server to run the application
- Board class used to add all elements required to display the board
- Game class to store information about the state of the game
- Classes for each piece describing them and their simple moving behaviours
- Add all pieces to the board in HTML
- [Jest](https://jestjs.io) unit tests for all typescript files which require 100% coverage
- Documentation for all typescript files using [TypeDoc](https://typedoc.org)
- Favicons of the pawn emoji
- GitLab CI pipelines to build, run tests, check code quality, and generate documentation
